@inject ('productImageHelper', 'Webkul\Product\Helpers\ProductImage')
@extends('admin::layouts.master')

@section('page_title')
    {{ __('dfm-quote::app.sales.quotes.view-title', ['quote_id' => $quote->id]) }}
@stop

@section('content-wrapper')

    <div class="content full-page">

        <div class="page-header">

            <div class="page-title">
                <h1>
                    <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                    {{ __('dfm-quote::app.sales.quotes.view-title', ['quote_id' => $quote->id]) }}
                </h1>
            </div>
        </div>

        <div class="page-content">

            <div class="sale-container">

                <accordian :title="'{{ __('dfm-quote::app.sales.quotes.quote-and-account') }}'" :active="true">
                    <div slot="body">

                        <div class="sale-section">
                            <div class="secton-title">
                                <span>{{ __('dfm-quote::app.sales.quotes.quote-info') }}</span>
                            </div>

                            <div class="section-content">
                                <div class="row">
                                    <span class="title">
                                        {{ __('dfm-quote::app.sales.quotes.quote-date') }}
                                    </span>

                                    <span class="value">
                                        {{ $quote->created_at }}
                                    </span>
                                </div>

                                <div class="row">
                                    <span class="title">
                                        {{ __('admin::app.sales.orders.channel') }}
                                    </span>

                                    <span class="value">
                                        {{ $quote->channel_name }}
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="sale-section">
                            <div class="secton-title">
                                <span>{{ __('admin::app.sales.orders.account-info') }}</span>
                            </div>

                            <div class="section-content">
                                <div class="row">
                                    <span class="title">
                                        {{ __('admin::app.sales.orders.customer-name') }}
                                    </span>

                                    <span class="value">
                                        {{ $quote->customer_full_name }}
                                    </span>
                                </div>

                                <div class="row">
                                    <span class="title">
                                        {{ __('admin::app.sales.orders.email') }}
                                    </span>

                                    <span class="value">
                                        {{ $quote->customer_email }}
                                    </span>
                                </div>

                                @if (! is_null($quote->customer))
                                    <div class="row">
                                        <span class="title">
                                            {{ __('admin::app.customers.customers.customer_group') }}
                                        </span>

                                        <span class="value">
                                            {{ $quote->customer->group['name'] }}
                                        </span>
                                    </div>
                                @endif
                            </div>
                        </div>

                    </div>
                </accordian>

                <accordian :title="'{{ __('admin::app.sales.orders.address') }}'" :active="true">
                    <div slot="body">

                        <div class="sale-section">
                            <div class="secton-title">
                                <span>{{ __('admin::app.sales.orders.billing-address') }}</span>
                            </div>

                            <div class="section-content">
                                @include ('admin::sales.address', ['address' => $quote->billing_address])
                            </div>
                        </div>

                        @if ($quote->shipping_address)
                            <div class="sale-section">
                                <div class="secton-title">
                                    <span>{{ __('admin::app.sales.orders.shipping-address') }}</span>
                                </div>

                                <div class="section-content">
                                    @include ('admin::sales.address', ['address' => $quote->shipping_address])
                                </div>
                            </div>
                        @endif

                    </div>
                </accordian>

                <accordian :title="'{{ __('dfm-quote::app.sales.quotes.shipping') }}'" :active="true">
                    <div slot="body">

                        @if ($quote->shipping_address)
                            <div class="sale-section">
                                <div class="secton-title">
                                    <span>{{ __('admin::app.sales.orders.shipping-info') }}</span>
                                </div>

                                <div class="section-content">
                                    <div class="row">
                                        <span class="title">
                                            {{ __('admin::app.sales.orders.shipping-method') }}
                                        </span>

                                        <span class="value">
                                            {{ $quote->selected_shipping_rate->carrier_title }} - {{ $quote->selected_shipping_rate->method_title }}
                                        </span>
                                    </div>

                                    <div class="row">
                                        <span class="title">
                                            {{ __('admin::app.sales.orders.shipping-price') }}
                                        </span>

                                        <span class="value">
                                            {{ core()->formatBasePrice($quote->selected_shipping_rate->base_price) }}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        @endif

                    </div>
                </accordian>

                <accordian :title="'{{ __('dfm-quote::app.sales.quotes.products-quoted') }}'" :active="true">
                    <div slot="body">

                        <div class="table">
                            <table>
                                <thead>
                                <tr>
                                    <th>{{ __('admin::app.sales.orders.SKU') }}</th>
                                    <th>{{ __('home.product.product-img') }}</th>
                                    <th>{{ __('admin::app.sales.orders.product-name') }}</th>
                                    <th>{{ __('admin::app.sales.orders.price') }}</th>
                                    <th>{{ __('admin::app.sales.orders.subtotal') }}</th>
                                    <th>{{ __('admin::app.sales.orders.tax-percent') }}</th>
                                    <th>{{ __('admin::app.sales.orders.tax-amount') }}</th>
                                    @if ($quote->base_discount_amount > 0)
                                        <th>{{ __('admin::app.sales.orders.discount-amount') }}</th>
                                    @endif
                                    <th>{{ __('admin::app.sales.orders.grand-total') }}</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach ($quote->items as $item)
                                    <?php $image = $productImageHelper->getProductBaseImage($item->product); ?>
                                    <tr>
                                        <td>
                                            {{ $item->sku }}
                                        </td>

                                        <td>
                                            @if(isset($item->additional['product_img']))
                                                <img src="{{ $item->additional['product_img'] }}" alt="image shopping cart">
                                            @else
                                                <img src="{{ $image['small_image_url'] }}" alt="{{ $item->name }}">
                                            @endif
                                        </td>

                                        <td>
                                            {{ $item->name }}

                                            @if (isset($item->additional['attributes']))
                                                <div class="item-options">

                                                    @foreach ($item->additional['attributes'] as $attribute)
                                                        <b>{{ $attribute['attribute_name'] }} : </b>{{ $attribute['option_label'] }}</br>
                                                    @endforeach

                                                </div>
                                            @endif
                                        </td>

                                        <td>{{ core()->formatBasePrice($item->base_price) }}</td>

                                        <td>{{ core()->formatBasePrice($item->base_total) }}</td>

                                        <td>{{ $item->tax_percent }}%</td>

                                        <td>{{ core()->formatBasePrice($item->base_tax_amount) }}</td>

                                        @if ($quote->base_discount_amount > 0)
                                            <td>{{ core()->formatBasePrice($item->base_discount_amount) }}</td>
                                        @endif

                                        <td>{{ core()->formatBasePrice($item->base_total + $item->base_tax_amount - $item->base_discount_amount) }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <table class="sale-summary">
                            <tr>
                                <td>{{ __('admin::app.sales.orders.subtotal') }}</td>
                                <td>-</td>
                                <td>{{ core()->formatBasePrice($quote->base_sub_total) }}</td>
                            </tr>

                            @if ($quote->haveStockableItems())
                                <tr>
                                    <td>{{ __('admin::app.sales.orders.shipping-handling') }}</td>
                                    <td>-</td>
                                    <td>{{ core()->formatBasePrice($quote->selected_shipping_rate->base_price) }}</td>
                                </tr>
                            @endif

                            @if ($quote->base_discount_amount > 0)
                                <tr>
                                    <td>
                                        {{ __('admin::app.sales.orders.discount') }}

                                        @if ($quote->coupon_code)
                                            ({{ $quote->coupon_code }})
                                        @endif
                                    </td>
                                    <td>-</td>
                                    <td>{{ core()->formatBasePrice($quote->base_discount_amount) }}</td>
                                </tr>
                            @endif

                            <tr class="border">
                                <td>{{ __('admin::app.sales.orders.tax') }}</td>
                                <td>-</td>
                                <td>{{ core()->formatBasePrice($quote->base_tax_total) }}</td>
                            </tr>

                            <tr class="bold">
                                <td>{{ __('admin::app.sales.orders.grand-total') }}</td>
                                <td>-</td>
                                <td>{{ core()->formatBasePrice($quote->base_grand_total) }}</td>
                            </tr>
                        </table>

                    </div>
                </accordian>

            </div>

        </div>

    </div>
@stop
