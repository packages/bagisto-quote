<?php

return [
    'datagrid'  => [
        'quote-date'    => 'Quote Date',
    ],
    'layouts'   => [
        'quotes'        => 'Quotes',
    ],
    'sales'     => [
        'quotes'        => [
            'title'             => 'Quotes',
            'view-title'        => 'Quote #:quote_id',
            'quote-and-account' => 'Quote and Account',
            'quote-info'        => 'Quote information',
            'quote-date'        => 'Quote date',
            'shipping'          => 'Shipping',
            'products-quoted'   => 'Products Quoted',
        ],
    ],
];
