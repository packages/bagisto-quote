<?php

return [
    [
        'key'        => 'sales.quotes',
        'name'       => 'dfm-quote::app.layouts.quotes',
        'route'      => 'admin.sales.quotes.index',
        'sort'       => 5,
        'icon-class' => '',
    ],
];
