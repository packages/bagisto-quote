<?php

namespace DFM\Quote\Models;

use Illuminate\Database\Eloquent\Builder;
use Webkul\Checkout\Models\Cart;
use Webkul\Checkout\Models\CartAddressProxy;
use Webkul\Checkout\Models\CartItemProxy;
use Webkul\Core\Models\ChannelProxy;
use Webkul\Customer\Models\CustomerProxy;

class Quote extends Cart
{
    /**
     * Get the items for the quote.
     */
    public function items()
    {
        return $this->hasMany(CartItemProxy::modelClass(), 'cart_id')->whereNull('parent_id');
    }

    /**
     * Get the addresses for the quote.
     */
    public function addresses()
    {
        return $this->hasMany(CartAddressProxy::modelClass(), 'cart_id');
    }

    /**
     * Get the channel that owns the quote.
     */
    public function channel()
    {
        return $this->belongsTo(ChannelProxy::modelClass());
    }

    /**
     * Get the customer that owns the quote.
     */
    public function customer()
    {
        return $this->belongsTo(CustomerProxy::modelClass());
    }

    /**
     * Get the channel's name.
     *
     * @return string
     */
    public function getChannelNameAttribute()
    {
        return $this->channel->name;
    }

    /**
     * Get the customer's full name.
     *
     * @return string
     */
    public function getCustomerFullNameAttribute()
    {
        return "{$this->customer_first_name} {$this->customer_last_name}";
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(function (Builder $builder) {
            $builder->where('is_quote', '=', true);
        });
    }
}
