<?php

namespace DFM\Quote\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DFM\Quote\Http\Controllers\Traits\Exportable;
use DFM\Quote\Models\Quote;

class QuoteController extends Controller
{
    use Exportable;

    /**
     * @var string
     */
    protected $exportableGrid = 'QuoteDataGrid';

    /**
     * Display a listing of the quote.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('dfm-quote::admin.index');
    }

    /**
     * Display the specified quote.
     *
     * @param  Quote  $quote
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Quote $quote)
    {
        return view('dfm-quote::admin.show', compact('quote'));
    }
}
