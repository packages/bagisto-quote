<?php

namespace DFM\Quote\DataGrids;

use DFM\Quote\Models\Quote;
use Illuminate\Support\Facades\DB;
use Webkul\Checkout\Models\CartAddress;
use Webkul\Ui\DataGrid\DataGrid;

class QuoteDataGrid extends DataGrid
{
    protected $index = 'id';

    protected $sortOrder = 'desc';

    public function prepareQueryBuilder()
    {
        $queryBuilder = DB::table('cart')
            ->leftJoin('addresses as quote_address_shipping', function($leftJoin) {
                $leftJoin->on('quote_address_shipping.cart_id', '=', 'cart.id')
                    ->where('quote_address_shipping.address_type', CartAddress::ADDRESS_TYPE_SHIPPING);
            })
            ->leftJoin('addresses as quote_address_billing', function($leftJoin) {
                $leftJoin->on('quote_address_billing.cart_id', '=', 'cart.id')
                    ->where('quote_address_billing.address_type', CartAddress::ADDRESS_TYPE_BILLING);
            })
            ->where('is_quote', '=', true)
            ->addSelect('cart.id', 'cart.base_sub_total', 'cart.base_grand_total', 'cart.created_at')
            ->addSelect(DB::raw('CONCAT(' . DB::getTablePrefix() . 'quote_address_billing.first_name, " ", ' . DB::getTablePrefix() . 'quote_address_billing.last_name) as billed_to'))
            ->addSelect(DB::raw('CONCAT(' . DB::getTablePrefix() . 'quote_address_shipping.first_name, " ", ' . DB::getTablePrefix() . 'quote_address_shipping.last_name) as shipped_to'));

        $this->addFilter('billed_to', DB::raw('CONCAT(' . DB::getTablePrefix() . 'quote_address_billing.first_name, " ", ' . DB::getTablePrefix() . 'quote_address_billing.last_name)'));
        $this->addFilter('shipped_to', DB::raw('CONCAT(' . DB::getTablePrefix() . 'quote_address_shipping.first_name, " ", ' . DB::getTablePrefix() . 'quote_address_shipping.last_name)'));
        $this->addFilter('id', 'cart.id');
        $this->addFilter('created_at', 'cart.created_at');

        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {
        $this->addColumn([
            'index'      => 'id',
            'label'      => trans('admin::app.datagrid.id'),
            'type'       => 'string',
            'searchable' => false,
            'sortable'   => true,
            'filterable' => true,
        ]);

        $this->addColumn([
            'index'      => 'base_sub_total',
            'label'      => trans('admin::app.datagrid.sub-total'),
            'type'       => 'price',
            'searchable' => false,
            'sortable'   => true,
            'filterable' => true,
        ]);

        $this->addColumn([
            'index'      => 'base_grand_total',
            'label'      => trans('admin::app.datagrid.grand-total'),
            'type'       => 'price',
            'searchable' => false,
            'sortable'   => true,
            'filterable' => true,
        ]);

        $this->addColumn([
            'index'      => 'created_at',
            'label'      => trans('dfm-quote::app.datagrid.quote-date'),
            'type'       => 'datetime',
            'sortable'   => true,
            'searchable' => false,
            'filterable' => true,
        ]);

        $this->addColumn([
            'index' => 'billed_to',
            'label' => trans('admin::app.datagrid.billed-to'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true,
        ]);

        $this->addColumn([
            'index' => 'shipped_to',
            'label' => trans('admin::app.datagrid.shipped-to'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true,
        ]);
    }

    public function prepareActions()
    {
        $this->addAction([
            'title'  => trans('admin::app.datagrid.view'),
            'method' => 'GET',
            'route'  => 'admin.sales.quotes.show',
            'icon'   => 'icon eye-icon',
        ]);
    }
}
